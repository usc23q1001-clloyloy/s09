from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict

# Create your views here.
def index(request):
	login_username = "MarvinNavarro"
	login_password = "Marvin1"
	user = authenticate(username=login_username, password=login_password)
	if user is not None:
		authenticated_user = User.objects.get(username='MarvinNavarro')
		is_user_authenticated = True
		context = {
			"first_name" : authenticated_user.first_name,
			"last_name" : authenticated_user.last_name,
			"is_user_authenticated": is_user_authenticated
		}
		return render(request, "user/index.html", context)
	else:
		return HttpResponse("You are accessing index page without logging in any account.")


def register(request):
	users = User.objects.all()
	is_user_registered = False

	context = {
	"is_user_registered" : is_user_registered
	}

	for indiv_user in users:
			if indiv_user.username == "MarvinNavarro":
				is_user_registered = True
				break

	if is_user_registered == False:
		user = User()
		user.username = "MarvinNavarro"
		user.first_name = "Marvin"
		user.last_name = "Navarro"
		user.email = "Marvin@mail.com"
		user.set_password("Marvin1234")
		user.is_staff = False
		user.is_active = True
		user.save()

		context = {
			"first_name" : user.first_name,
			"last_name" : user.last_name,
		}

	return render(request, "user/register.html", context)


def change_password(request):
	is_user_authenticated = False

	user = authenticate(username="MarvinNavarro", password="Marvin1234")
	print(user)

	if user is not None:
		authenticated_user = User.objects.get(username='MarvinNavarro')
		authenticated_user.set_password("Marvin1")
		authenticated_user.save()
		is_user_authenticated = True
		context = {
			"username" : authenticated_user.username,
			"is_user_authenticated" : is_user_authenticated
		}
	else:
		context = {
			"is_user_authenticated" : is_user_authenticated
		}

	return render(request, "user/change_password.html", context)


def login_view(request):
	login_username = "MarvinNavarro"
	login_password = "Marvin1"
	user = authenticate(username=login_username, password=login_password)
	print(user)

	if user is not None:
		login(request,user)
		return redirect("index")

	else:
		context = {
			"is_user_authenticated" : False
		}
		return render(request, "user/login.html", context)


def logout_view(request):
	logout(request)
	return redirect("index")