from django.apps import AppConfig


class UserPracticeAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'user_practice_app'
